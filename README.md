# Introduction to proteomics - practical

[[_TOC_]]

### Pre-requisites

The exercises require the conda package manager and the bioconda repository, which can be installed
following [these instructions](http://bioconda.github.io/).

It is strongly recommended that you install the `mamba` command which replaces `conda` for installing
and removing packages, and is much more efficient.

You should use a new, empty conda environment for these exercises. To create
an environment called "prot" you can use the following command:

```shell
$ conda install -y mamba # install the far superior mamba binary
$ mamba create -y -n prot
```

After creating it, and every time you wish to use it, you can activate the environment by executing

```shell
$ conda activate prot
```

If you don't have the `git` or `wget` commands in your system you can install them with mamba:

```shell
$ mamba install git wget
```

You can now clone this repository to get some auxiliary files:

```shell
$ git clone https://gitlab.com/bioinfo-lessons/intro-proteomics.git
$ cd intro-proteomics
```

### Exercise: Bacterial data

#### 1. Download raw proteomics data

We'll download a dataset from the [PXD008787 project](https://www.ebi.ac.uk/pride/archive/projects/PXD008787) in the PRIDE repository.

```shell
$ cat data/PXD008787/files
$ wget -i data/PXD008787/files -P data/PXD008787
```

#### 2. Install ProteoWizard

Proteowizard includes the "msconvert" command, for format conversion.

```shell
$ mamba install -y proteowizard
```

#### 3. Use msconvert to convert the sample file to some of the different available formats

See msconvert's help to understand the syntax to use.
```shell
$ msconvert --help
```
Convert one of the mgf files to a few other formats.
```shell
$ msconvert data/PXD008787/20150410_QE3_UPLC9_DBJ_SA_46fractions_Rep1_1_MGF.mgf -o data/PXD008787/ --mzML
$ msconvert data/PXD008787/20150410_QE3_UPLC9_DBJ_SA_46fractions_Rep1_1_MGF.mgf -o data/PXD008787/ --mzXML
$ msconvert data/PXD008787/20150410_QE3_UPLC9_DBJ_SA_46fractions_Rep1_1_MGF.mgf -o data/PXD008787/ --text
```

#### 4. Take a look at the content of .mgf, .txt, .mzXML and .mzML files and try to understand how they differ (broadly).

.mgf and .txt files are semi-structured text files, with custom formats. You would need to have previous knowledge
about the format in order to parse the data.

.mzML and .mzXML files are structured files, using subsets of XML (eXtensible Markup Language).

All of them contain the same information represented in different ways. Markup files take up more space
by using opening and closing tags, but in exchange are much easier to parse.

#### 5. Download the E. coli proteome

You can download a complete proteome from [UniProt](https://www.uniprot.org/proteomes/UP000000625) or the [NCBI](https://www.ncbi.nlm.nih.gov/genome/167?genome_assembly_id=161521).

```shell
$ mkdir -p resources/proteome
$ wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/reference_proteomes/Bacteria/UP000000558/UP000000558_83334.fasta.gz -O resources/proteome/ecoli.fasta.gz
$ gunzip resources/proteome/ecoli.fasta.gz
```

#### 6. Use decoypyrat to generate a decoy database.

```shell
$ mamba install -y decoypyrat
```

Invoke the help of decoypyrat to see available options.

```shell
$ decoypyrat --help
```

Use the "decoy_prefix" to set the prefix to "DECOY". This will allow us to easily
identify decoy assignments downstream.

```shell
$ decoypyrat resources/proteome/ecoli.fasta -o resources/proteome/ecoli.decoy.fasta --decoy_prefix DECOY
```

Merge targets and decoys

```shell
$ cat resources/proteome/ecoli.fasta resources/proteome/ecoli.decoy.fasta > resources/proteome/ecoli.target-decoy.fasta
```

#### 7. Install the crux toolkit.

```shell
$ mamba install -y crux-toolkit
```

The [crux documentation](http://crux.ms) is really good. Be sure to check it for help
with the different commands.

#### 8. Run comet to search our input data against the target-decoy database
    
```shell
$ crux comet --output-dir output/comet --overwrite T --output_txtfile 0 data/PXD008787/*.mgf resources/proteome/ecoli.target-decoy.fasta
```

#### 9. Explore one of the output files.

Comet outputs search results in .pep.xml format. This is an XML-formatted text file you can open with any editor.

Among other things, the file will contain one "spectrum_query" tag for each searched spectrum. In turn, each
"spectrum_query" tag will contain multiple "search_hit" tags representing the peptides that comet thinks may be
a match for the spectrum.

Here is a simplified example of how the tags that represent spectra and their hits look:

```xml
    <spectrum_query spectrum="spectrum1">
        <search_hit hit_rank="1">
            <search_score name="xcorr" value="1.081"/>
        </search_hit>
        <search_hit hit_rank="2">
            <search_score name="xcorr" value="0.955"/>
        </search_hit>
        <search_hit hit_rank="3">
            <search_score name="xcorr" value="0.845"/>
        </search_hit>
    </spectrum_query>
    <spectrum_query spectrum="spectrum2">
        <search_hit hit_rank="1">
            <search_score name="xcorr" value="2.155"/>
        </search_hit>
        <search_hit hit_rank="2">
            <search_score name="xcorr" value="0.355"/>
        </search_hit>
    </spectrum_query>
```

In this example, spectrum1 has 3 hits reported and spectrum2 has two hits reported. Each hit has an associated
score, in the form of an xcorr value.

Looking at the real file contents, notice how each search_hit represents a completely different peptide (by
looking at the "peptide" attribute) that the search engine considers could be a match. This is due to the
difficulty in determining the right peptide only from its mass.

Take a look at the "protein" attribute and see if you can detect any DECOYS that were assigned to spectra.

#### 10. Convert one of the comet output files to tsv using crux psm-convert.

In order to make it easier for a human being to read the data in the output files, let's convert one of them
to a good-old tab separated file.

```shell
$ crux psm-convert --output-dir output/comet --overwrite T output/comet/comet.20150410_QE3_UPLC9_DBJ_SA_46fractions_Rep1_1_MGF.target.pep.xml tsv
```

Let's give the converted file a more sensible name
```shell
$ mv output/comet/psm-convert.txt output/comet/comet.20150410_QE3_UPLC9_DBJ_SA_46fractions_Rep1_1_MGF.target.tsv
```

Take a look at the tsv file. It will contain the same information as the .pep.xml file, with one line for each
search_hit.

#### 11. Estimate FDR (spreadsheet or any programming language)

To estimate the false discovery rate of the data in the tsv file, we will use a simple algorithm in a spreadsheet.

Open the tsv file in your spreadsheet software. Make sure to set the import language to English, and the column separator
to tab *ONLY* (disable any other separator options).

To estimate FDR at each row:
- Sort by the chosen score (e.g. xcorr) in descending order
- Create a "decoy indicator" column where cells have value 1 if the hit is a decoy. Assuming that your
"protein id" data is in column "P", you could use a formula like "=IF(ISNUMBER(FIND("DECOY",P2)),1,0)"
to get a "1" if the column contains the word "DECOY", and a "0" otherwise.
- Create a column where cells have the cumulative sum of "decoy indicator". Assuming that "decoy indicator"
is in column S, you can use the formula "=SUM(S$2:S2)" in the first cell, and drag the formula to the 
rest of the cells in the column.
- Create a "target indicator" column where cells have value 1 if the hit is a target (you can use the same
formula as in b), but swapping the 1 and the 0.
- Create a column where cells have the cumulative sum of "target indicator" (analogous to bullet point #3 above).
- Create the FDR column by dividing the decoy cumulative sum by the target cumulative sum.

NOTE: there is a sample FDR calculation spreadsheet in the "output/comet" directory.

#### 12. Calculate confidence with crux assign-confidence.

```shell
$ crux assign-confidence --output-dir output/assign-confidence --overwrite T --decoy-prefix DECOY_ output/comet/*.pep.xml
```

#### 13. Calculate confidence with crux percolator.

```shell
$ crux percolator --output-dir output/percolator --overwrite T --decoy-prefix DECOY_ output/comet/*.pep.xml
```

#### 14. Compare results

We've generated FDR (or the equivalent q-value) statistics for our data using three methods of increasing complexity:
manual calculations, the "assign-confidence" method, and the "percolator" method.

Given that this exercise is conducted on a small amount of data, without much care to the optimisation of the parameters
of the search and/or FDR calculations, you will not see any significant values on any of the methods. You can, however,
try to see how they compare against each other, seeing how many results you would keep on each one given an arbitrary
FDR value of your choice.

Be aware that you should convert all the comet output files to tsv and merge them all into a single file before calculating
the FDR in order for the results to be comparable to the other two algorithms, which merge all the output together.

One interesting trick is to "simulate" better results in the manual calculation by removing the protein labels
from the first n rows (start with, say, 100). This will make the formula count all those as targets, and the FDR will get much better
until the point where you removed the labels. After this point, the FDR will start to quickly rise again.

---

### Assignment: Human data 

#### If you're taking a class...

If you're taking this lesson as part of a class, this will be the assignment that you need to 
hand in.

You should hand in:
- a bash script containing the commands you run (you can use comments for any relevant notes)
- a text file with some notes on the results comparison (point 7)

#### Exercise

Try now processing Human data (which usually involves much larger volumes),
following these less detailed instructions.

1. Download the human proteome
2. Create a target-decoy database
3. Download a human dataset from PRIDE
4. Run comet
    - Tune parameters to use one core less than what you machine has ("cat /proc/cpuinfo | grep -c processor")
    - Use small batches of spectra (~5000)
5. Calculate confidence with assign-confidence
6. Calculate confidence with percolator
7. Compare results

#### Bonus

These two optional exercises are a bit more challenging, since they require you to do a bit of research to find the right options.

8. Repeat the analysis using the [crux pipeline](http://crux.ms/commands/pipeline.html)
9. Install the tpp pipeline and use PeptideProphet and ProteinProphet to group peptides into protein

---

### Extracting all commands into a script

Not recommended the first time you go through the tutorial, but you can clone this repository and extract all the commands in this file with the following script:

```shell
$ grep "^\\$" README.md | grep -v "intro_proteomics.sh" | sed 's/^\$ //' | sed 's/conda activate/source activate/' > intro_proteomics.sh
```
You can then execute the resulting script like this

```shell
$ source intro_proteomics.sh
```
